FROM docker:latest

COPY package-list.txt /.
COPY repository-list.txt /.

RUN cp /repository-list.txt /etc/apk/repositories
RUN apk update
RUN apk add --no-cache findutils
RUN xargs apk add --no-cache < /package-list.txt

